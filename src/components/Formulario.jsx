import {useState, useEffect} from 'react'
import Error from './Error';

function Formulario({pasientes, setPasientes, pasiente, setPasiente}) { 
    const [nombre, setNombre] = useState('');
    const [propietario, setPropietario] = useState('');
    const [email, setEmail] = useState('');
    const [fecha, setFecha] = useState('');
    const [sintomas, setSintomas] = useState('');

    const [error, setErro] = useState(false);

    useEffect (() =>{
        if(Object.keys(pasiente).length > 0){
            setNombre(pasiente.nombre);
            setPropietario(pasiente.propietario);
            setEmail(pasiente.email);
            setFecha(pasiente.fecha);
            setSintomas(pasiente.sintomas);
        }
    }, [pasiente])

    function generateId(){
        const random = Math.random().toString(36).substring(2);
        const fecha = Date.now().toString(36);

        return random + fecha;
    }

    const handleSubmit = (e) =>{
        e.preventDefault();

        if([nombre, propietario, email, fecha, sintomas].includes('')){
            setErro(true);
            return;
        }
        setErro(false);

        const objectPasiente =  {
            nombre, 
            propietario, 
            email, 
            fecha, 
            sintomas,
        }

       if(pasiente.id){
           objectPasiente.id = pasiente.id
           const updatePasiente = pasientes.map(pasienteState => pasienteState.id === pasiente.id ? objectPasiente : pasienteState)
           setPasientes(updatePasiente)
           setPasiente({})
       }else{
           objectPasiente.id = generateId();
            setPasientes([...pasientes, objectPasiente]);
       }


        //reset form
        setNombre('');
        setPropietario('');
        setEmail('');
        setFecha('');
        setSintomas('');

    }


  return (
    <div className="md:w-1/2 lg:w-2/5 ">
      <h2 className='font-black text-center text-3xl'>Seguimiento paciente</h2>
      <p className='mt-5 text-lg text-center mb-5'>
        Añade Paciente y{" "}
        <span className='text-indigo-600 font-bold'>Administralos</span>
      </p>

      <form 
        className='bg-white shadow-md rounded-lg py-10 px-5'
        onSubmit={handleSubmit}
        >
            {error && 
                <Error>Todos los campos Son obligatorios</Error>
            }
            <div className='mb-5'>
                <label htmlFor='mascota' className='block text-gray-700 uppercase font-bold'>Nombre Mascota</label>
                <input 
                    type="text"
                    placeholder='Nombre de la mascota'
                    className='border-2 w-full p-2 mt-2 placeholder-gray-600 rounded-md'
                    id='mascota'
                    value={nombre}
                    onChange={(e)=> {setNombre(e.target.value)}}
                />
            </div>
            <div className='mb-5'>
                <label htmlFor='propietario' className='block text-gray-700 uppercase font-bold'>Nombre del Propietario</label>
                <input 
                    type="text"
                    placeholder='Nombre del propietario'
                    className='border-2 w-full p-2 mt-2 placeholder-gray-600 rounded-md'
                    id='propietario'
                    value={propietario}
                    onChange={(e)=> {setPropietario(e.target.value)}}
                />
            </div>
            <div className='mb-5'>
                <label htmlFor='email' className='block text-gray-700 uppercase font-bold'>Email</label>
                <input 
                    type="email"
                    className='border-2 w-full p-2 mt-2 placeholder-gray-600 rounded-md'
                    id='email'
                    value={email}
                    onChange={(e)=> {setEmail(e.target.value)}}
                    placeholder="correo@correo.com"
                />
            </div>
            <div className='mb-5'>
                <label htmlFor='alta' className='block text-gray-700 uppercase font-bold'>Alta</label>
                <input 
                    type="date"
                    className='border-2 w-full p-2 mt-2 placeholder-gray-600 rounded-md'
                    id='alta'
                    value={fecha}
                    onChange={(e)=> {setFecha(e.target.value)}}
                />
            </div>
            <div className='mb-5'>
                <label htmlFor='sintomas' className='block text-gray-700 uppercase font-bold'>Sintomas</label>
             <textarea id="sintomas" cols="30" rows="10"
              placeholder='Descripcion de los sintomas'
              className='border-2 w-full p-2 mt-2 placeholder-gray-600 rounded-md'
                value={sintomas}
                onChange={(e)=> {setSintomas(e.target.value)}}
             ></textarea>
            </div>
            <input type="submit" 
                className='bg-indigo-600 w-full p-3 text-white font-bold hover:bg-indigo-700 cursor-pointer'
                value={pasiente.id ? "Editar pasinete" : "Agregar Paciente"}
            />
        </form>
    </div>
  )
}

export default Formulario