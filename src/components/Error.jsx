
const Error = ({children}) => {
  return (
    <div>
        <p className='bg-red-800 text-center uppercase w-full p-3 font-bold mb-3 rounded-md text-white'>
            {children}
        </p>
    </div>
  )
}

export default Error