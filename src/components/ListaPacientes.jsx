import Pasientes from './Pasientes';

const ListaPacientes = ({pasientes, setPasiente,eliminarPasiente}) => {
  return (
    <div className="md:w-1/2 lg:w-3/5 md:h-screen overflow-y-scroll mt-10 md:mt-0">
      {pasientes && pasientes.length
      ?(
        <>
          <h2 className="text-center font-balck text-3xl">
          Lista Pacientes
          </h2>
          <p className="text-center text-lg mt-5 mb-5">
              Administra tus {" "}
              <span className="text-indigo-600 font-bold">Pacientes y Citas</span>
          </p>
          {
            pasientes.map(pasiente => 
              <Pasientes
                key={pasiente.id}
                pasiente={pasiente}
                setPasiente={setPasiente}
                eliminarPasiente={eliminarPasiente}
              />
            )
          }
        </>
      )
      :(
        <>
          <h2 className="text-center font-balck text-3xl">
          No hay Pasientes
          </h2>
          <p className="text-center text-lg mt-5 mb-5">
              Ingresa un pasinete {" "}
              <span className="text-indigo-600 font-bold">y aparecera en este lugar</span>
          </p>
        </>
      )}
    </div>
  )
}

export default ListaPacientes