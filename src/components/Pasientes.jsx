import React from 'react'

const Pasientes = ({pasiente,setPasiente,eliminarPasiente}) => {
    const {nombre, propietario, email, fecha, sintomas, id} = pasiente;

    const handleDelete = () => {
        const response = confirm('Desea eliminar este pasiente')
        if(response){
            eliminarPasiente(id);
        }
    }

  return (
     <div className="m-3 bg-white shadow-md rounded-xl px-5 py-10">
        <p className="font-bold mb-3 text-gray-700 uppercase">
            Nombre: {" "}
            <span className="font-normal normal-case">{nombre}</span>
        </p>
        <p className="font-bold mb-3 text-gray-700 uppercase">
            Propietario: {" "}
            <span className="font-normal normal-case">{propietario}</span>
        </p>
        <p className="font-bold mb-3 text-gray-700 uppercase">
            Email: {" "}
            <span className="font-normal normal-case">{email}</span>
        </p>
        <p className="font-bold mb-3 text-gray-700 uppercase">
            Alta: {" "}
            <span className="font-normal normal-case">{fecha}</span>
        </p>
        <p className="font-bold mb-3 text-gray-700 uppercase">
            Sintomas: {" "}
            <span className="font-normal normal-case">{sintomas}</span>
        </p>

        <div className='flex justify-between'>
            <button
                type='button'
                className='bg-indigo-600 hover:bg-indigo-700 text-center text-white font-bold uppercase py-2 px-10 rounded-md'
                onClick={()=>{setPasiente(pasiente)}}
            >Editar</button>

            <button
                type='button'
                className='bg-red-600 hover:bg-red-700 text-center text-white font-bold uppercase py-2 px-10 rounded-md'
                onClick={handleDelete}
            >Eliminar</button>
        </div>
    </div>
  )
}

export default Pasientes