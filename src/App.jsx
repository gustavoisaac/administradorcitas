import { useState, useEffect } from 'react';
import React from 'react';
import Header from "./components/Header";
import Formulario from './components/Formulario';
import ListaPacientes from './components/ListaPacientes';

function App() {
  const [pasientes, setPasientes] = useState([])
  const [pasiente, setPasiente] = useState({})

  useEffect(() => {
    const getSorage = JSON.parse(localStorage.getItem('pasientes')) ?? [];
    setPasientes(getSorage)
  }, []);
  
  useEffect(() => {
    if(pasientes.length > 0){
      localStorage.setItem('pasientes', JSON.stringify( pasientes ))
    }
  }, [pasientes])

  const eliminarPasiente = (id) => {
    const updatePasiente = pasientes.filter(pasiente => pasiente.id != id);
    setPasientes(updatePasiente)
  }


  return (
    <div className="container mx-auto mt-10">
       <Header
      
       />
       
       <div className="mt-12 md:flex">
          <Formulario
              pasientes={pasientes}
              setPasientes={setPasientes}
              pasiente={pasiente}
              setPasiente={setPasiente}
          />
          <ListaPacientes
            pasientes={pasientes}
            setPasiente={setPasiente}
            eliminarPasiente={eliminarPasiente}
          />
        </div>

    </div>
  )
}

export default App;
